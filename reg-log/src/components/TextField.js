import { useField, ErrorMessage } from 'formik';
import React from 'react'

const TextField = ({label, ...props}) => {

    const [fields, meta]= useField(props);
    // console.log(field, meta);
    return (
        <div>
        <label htmlFor={fields.name}>{label}</label>
            <input 
                className={`form-control shadow ${meta.touched && meta.error && "is-invalid"} `}
                {...fields} {...props}
                autoComplete='off'
            />
            <ErrorMessage component="p" name={fields.name} />
        </div>
    )
}

export default TextField;