import React from 'react'

import {Formik, Form} from "formik";
import * as Yup from "yup";
import TextField from "./TextField";
import imgg from "./imagetoview.png"


const SignUp = () => {
    const validateUser = Yup.object({
      firstName: Yup.string()
      .max(12, "First Name should be 12 or less")
      .required("Required"),

      lastName: Yup.string()
      .max(12, "Last Name should be 12 or less")
      .required("Required"),

      email: Yup.string()
      .email("Email is invalid")
      .required("Required"),

      phone: Yup.string()
      // .typeError("Input must be Number")
      .matches(/^[0-9]+$/, "Input must be Number")
      .min(10, "Phone number is less than 10 digit")
      .max(10, "Phone number is more than 10 digit")
      .required("Required"),


      country: Yup.string()
      .max(20, "Country Name is required")
      .required("Required"),

      address: Yup.string()
      .max(25, "Address should be 25 or less")
      .required("Required"),

      workingEmail: Yup.string()
      .email("Working Email domain is invalid")
      .matches(/.+@logic-square.com/, "Working domain not matched")
      .max(25, "Working Email should be 25 or less")
      .required("Required"),

      branch: Yup.string()
      .max(15, "Branch Name should be 15 or less")
      .required("Required"),

      supervisor: Yup.string()
      .max(15, "Supervisor should be 12 or less")
      .required("Required"),

      password: Yup.string()
      .min(8, "Password length should minimum 8")
      .required("Password is Required"),

      confirmPassword: Yup.string()
      .oneOf([Yup.ref("password"), null], "Password not matched")
      .required("Re-enter the Required"),
    }) 
  return (
    <div className="container mt-5 mx-auto">
        <div className="row">
          <div className='col-md-4 mx-3 my-auto mx-auto'>
            <img src={imgg} alt="..." />
          </div>
          <div className='col-md-7 mx-3'>
          <Formik
          initialValues = {{
           firstName: "",
            lastName: "",
            email: "",
            phone: "",
            country: "",
            address: "",
            workingEmail: "",
            branch: "",
            supervisor: "",
            password: "",
            confirmPassword: "",
          }}
          validationSchema={validateUser}
          onSubmit={(values) => console.log(values)}
          >
            {(formik) => (
              <div>
            {/* {console.log(formik.values)} */}
                <h1 className="my-4 font-weight-bold-display-4">SignUp</h1>
                <Form>
                  <TextField label="First Name" name="firstName" type="text" />
                  <TextField label="Last Name" name="lastName" type="text" />
                  <TextField label="Email" name="email" type="email" />
                  <TextField label="Phone" name="phone" type="tel" />
                  <TextField label="Country" name="country" type="text" />
                  <TextField label="Address" name="address" type="text" />
                  <TextField label="Working Email" name="workingEmail" placeholder="name@logic-square.com" type="text" />
                  <TextField label="Branch" name="branch" type="text" />
                  <TextField label="Supervisor" name="supervisor" type="text" />
                  <TextField label="Password" name="password" type="password" />
                  <TextField label="Confirm Password" name="confirmPassword" type="password" />
                  <button className="btn btn-primary mt-5" type='submit'>Submit</button>
                  <button className="btn btn-danger mt-5" type="reset">reset</button>
                </Form> 
              </div>
            )}
    
        </Formik>


          </div>
        </div>
      </div>











    
  )
}

export default SignUp